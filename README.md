# Genera Exercise Smart Contract 
##### Goal: Evaluate knowledge of the Ethereum environment and related libraries.
This repository presents practices dedicated to Genera test exercise:

- Setup a Blockchain.
- Develop Ethereum smart contract.
- Deploy the contract and test it.
- Runs the client, sending transaction to the blockchain through the Smart Contract.
- The client send a raw signed transaction, that represents a Candidate entity(struct). Only Candidates with the best scores will be accepted.
- After sent, we can get the Candidate from it.

## Installation

### Setup

- **Truffle**
    
    `sudo npm i -g truffle@5.4.1`


Clone the repository

        git clone https://gitlab.com/Guima/genera-exercise.git

Start your Blockchain Ethereum, I used the truffle develop environment to simulate the blockchain
    
    cd genera-exercise/
    truffle develop startup environment

But you can use **Ganache**, installation guide can be found [here](https://www.trufflesuite.com/ganache).
If you using Ganache, maybe must change `truffle-config.js` network values.

then compile the Smart Contract using truffle:
    
    truffle compile

After compile the project and starting truffle environment or ganache which provides ethereum blockchain network on your local, our smart contract can be deployed using truffle:

    truffle migrate
*Sometimes need to run it with the flag `--reset`, replacing an older version of the contract.*

Now, our smart contract has been compiled and deployed, so we have 3 ways to test it:
- **truffle console**
  
    `truffle console` then:

    `var instance;`

    `Exercise.deployed().then((i)=>{instance = i})`

    `instance.printHello.call() // prints Hello Candidate!`

    `instance.setName("Guilherme")`

    `instance.printHello.call() // prints Hello Guilherme!`


- **unit test**
  
    `truffle test`
- **client nodejs project** 

    To test the real Genera test exercise. (see above)
    



## Client 
#### Genera Exercise
NodeJS client project using web3js to access the smart contract, sending and querying transactions.
- Statement: Develop a smart contract with a method that receives an array of bytes and that its execution has a cost of between 100,000 and 120,000 gas units.
- Running, it starts a nodejs server inserting a new Candidate to blockchain.   
- The application calls a smart contract that receive array of bytes and save it.
- Only operation with cost of between 100,000 and 120,000 gas will be accepted.
- Only New Candidate or one with a best score will be accepted.
- To change the current candidate value in blockchain, just update the `./client/candidate-mock.js`


Before you run the application, is necessary to check and change your environment variables, just open file 
  `./client/.env`

    BLOCKCHAIN_URL=ws://localhost:9545 or ${your_ethereum_blockchain_url}
    PUBLIC_KEY=${your_public_key_address}
    PRIVATE_KEY=${your_private_key}

Enter in client folder and install dependencies

    cd client
    npm i

Then, to run and test the application just:

    yarn test


## Author
Guilherme Palmeira
