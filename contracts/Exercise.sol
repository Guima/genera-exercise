pragma solidity ^0.5.0;

contract Exercise {
    struct Candidate {
        bytes bytePerson;
    }
    Candidate[] candidates;
    bytes candidate;
    string name = "Candidate";

    function addCandidate(bytes memory _byteCandidate) public {
        Candidate memory candidate = Candidate(_byteCandidate);
        candidates.push(candidate);
    }

    function getQtyCandidates() public returns(uint count) {
        return candidates.length;
    }

    function setCandidate(bytes memory _byteCandidate) public {
        candidate = _byteCandidate;
    }

    function getCandidate() public returns(bytes memory) {
        return candidate;
    }

    function setName(string memory _name) public{
        name = _name;
    }

    function printHello() public returns(string memory){
        bytes memory concat_strings;
        concat_strings = abi.encodePacked("Hello ");
        concat_strings = abi.encodePacked(concat_strings, name);
        concat_strings = abi.encodePacked(concat_strings, "!");
        string memory msg = string(concat_strings);
        
        return msg;
    }

}
