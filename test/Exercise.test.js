const Exercise = artifacts.require('Exercise');

contract('Exercise', function() {

	it("should print out hello default", function(){
		return Exercise.deployed().then(function(instance) {
			return instance.printHello.call();
		}).then(function(msg) {
			assert.equal(msg, "Hello Candidate!", "should print out hello to default name");
		});
	});
	
	it("should print out hello entered name", function(){
		return Exercise.deployed().then(async function(instance) {
			const name = "Guilherme";
			await instance.setName(name);
			return instance.printHello.call();
		}).then(function(msg) {
			assert.equal(msg, `Hello Guilherme!`, "should print out hello to entered name");
		});
	});
});


