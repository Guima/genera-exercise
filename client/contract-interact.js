const Web3 = require('web3');
let web3 = new Web3(Web3.givenProvider || process.env.BLOCKCHAIN_URL || "ws://localhost:9545");

const Candidate = require('./candidate-mock');
const contract = require("../build/contracts/Exercise.json"); 
const contractAddress = contract.networks['5777']['address']// = "0xcE958BD2a16C7a7146f33B52adaF4E9836b12bd8";
const myAddress = process.env.PUBLIC_KEY || "0x242d25758a85b0f9e0879074ece0ab224f089def"; // public key
const PRIVATE_KEY = process.env.PRIVATE_KEY || "841eb8520dd99fd0c474130525329a4cac995e0358bdfe835ef295596e37b8e3";
const mainContract = new web3.eth.Contract(contract.abi, contractAddress);


async function main() {
    try {
        let candidate = await getCandidate();
        if(!candidate || isNewCandidateBestScore(Candidate, candidate)){
            await setCandidate();
            candidate = await getCandidate();
        } else{
            console.log(`We already have a candidate with a best score: ${candidate.name}, score: ${candidate.score}`);
        }
        console.log("Candidate: ", Candidate);
    } catch (error) {
        console.log(error);
    }
}

const setCandidate = async () => {
    const hexCandidate = encode(JSON.stringify(Candidate));
    const fn = mainContract.methods.setCandidate;
    await sendHexTransaction(hexCandidate, fn);
}

const getCandidate = async () =>{
    const encodedCandidate = await mainContract.methods.getCandidate().call();
    let candidate;
    if(encodedCandidate){
        candidate = decode(encodedCandidate);
        candidate = JSON.parse(candidate);
    }
    return candidate;
}

async function sendHexTransaction(hexMetadata, fn) {
    const nonce = await web3.eth.getTransactionCount(myAddress, 'latest'); // get latest nonce
    const gasEstimate = await fn(hexMetadata).estimateGas(); // estimate gas
    console.log(nonce);
    console.log(gasEstimate);
    if(gasEstimate >= 100000 || gasEstimate <= 120000){
        console.log("Gas OK, sending transaction...");
    } else{
        throw new Error("Gas is not between 100000 and 120000");
    }
    
    // Create the transaction
    const tx = {
      'from': myAddress,
      'to': contractAddress,
      'nonce': nonce,
      'gas': gasEstimate, 
      'data': fn(hexMetadata).encodeABI()
    };

    // Sign and send the transaction
    await sendTransacion(tx);
}


main();

function decode(encodedCandidate){
    return web3.utils.hexToString(encodedCandidate);
}

function encode(strCandidate){
    return web3.utils.toHex(strCandidate);
}

async function sign(tx){
    return await web3.eth.accounts.signTransaction(tx, PRIVATE_KEY);
}
async function sendRawTransaction(signedTx){
    try {
        const hash = await web3.eth.sendSignedTransaction(signedTx.rawTransaction)
        console.log("The hash of your transaction is: ", hash, "\n Check the status of your transaction!");
        return hash;
    } catch (error) {
        console.log("Something went wrong when submitting your transaction:", error);
    }
}

function isNewCandidateBestScore(newCandidate, candidate){
    return newCandidate.score > candidate.score;
}

async function sendTransacion(tx){
    const signedTx = await sign(tx);
    await sendRawTransaction(signedTx)
}